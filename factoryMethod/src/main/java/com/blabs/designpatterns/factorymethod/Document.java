package com.blabs.designpatterns.factorymethod;

public class Document {
	
	protected void open() {
		System.out.println("Opening doc from storage.");
	}
	
	protected void close() {
		System.out.println("Closing doc.");
	}
	
	protected void save() {
		System.out.println("Saving doc into storage.");
	}
	
	protected void revert() {
		System.out.println("Reverting doc to previus version.");
	}
}
