package com.blabs.designpatterns.factorymethod;

import java.util.ArrayList;
import java.util.List;

public abstract class Application {
	protected List<Document> documents;
	
	public Application() {
		documents = new ArrayList<Document>();
	}
	
	// create document is the so called factory method
	protected abstract Document createDocument();
	
	public Integer newDocument() {
		Document doc = this.createDocument();
		this.documents.add(doc);
		
		doc.open();
		
		return this.documents.indexOf(doc);
	}
	
	public Document openDocument(Integer docId) {
		return this.documents.get(docId);
	}
}
