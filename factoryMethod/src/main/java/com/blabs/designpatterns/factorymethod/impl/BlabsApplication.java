package com.blabs.designpatterns.factorymethod.impl;

import com.blabs.designpatterns.factorymethod.Application;
import com.blabs.designpatterns.factorymethod.Document;

public class BlabsApplication extends Application {

	@Override
	protected Document createDocument() {
		Document blabsDocument = new BlabsDocument();
		
		return blabsDocument;
	}
}
