package com.blabs.designpatterns.factorymethod.impl;

import junit.framework.Assert;

import org.junit.Test;

import com.blabs.designpatterns.factorymethod.Application;
import com.blabs.designpatterns.factorymethod.Document;

public class BlabsApplicationTest {

	private Application blabsApp = new BlabsApplication();
	
	@Test
	public void createDocument_verifyWhetherBlabsDocumentCreated() {
		// Arrange
		Integer docId = this.blabsApp.newDocument();

		// Act
		Document doc = this.blabsApp.openDocument(docId);
		
		// Assert
		Assert.assertTrue(doc instanceof BlabsDocument);
	}
}
