package com.blabs.designpatterns.composite;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.blabs.designpatterns.composite.impl.Circle;
import com.blabs.designpatterns.composite.impl.Line;
import com.blabs.designpatterns.composite.impl.Picture;
import com.blabs.designpatterns.composite.impl.Rectangle;
import com.blabs.designpatterns.composite.impl.Text;

public class GraphicTest {
	
	private Graphic compositeRoot;
	@Mock
	private Line line1;
	@Mock
	private Text text1;
	@Mock
	private Circle circle1;
	@Mock
	private Rectangle rectangle1;

	private Picture picture;
	@Mock
	private Line line2;
	@Mock
	private Text text2;
	@Mock
	private Circle circle2;
	@Mock
	private Rectangle rectangle2;
	
	private Line line;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		
		line = new Line();
		
		compositeRoot = new Picture();
		
		compositeRoot.add(line1);
		compositeRoot.add(text1);
		compositeRoot.add(circle1);
		compositeRoot.add(rectangle1);
		
		picture = new Picture();
		compositeRoot.add(picture);
		
		picture.add(line2);
		picture.add(text2);
		picture.add(circle2);
		picture.add(rectangle2);
	}
	
	@Test
	public void draw_testDrawingTheWholeTree() {
		compositeRoot.draw();
		
		verify(line1, times(1)).draw();
		verify(circle1, times(1)).draw();
		verify(rectangle1, times(1)).draw();
		verify(text1, times(1)).draw();
		
		verify(line2, times(1)).draw();
		verify(circle2, times(1)).draw();
		verify(rectangle2, times(1)).draw();
		verify(text2, times(1)).draw();
	}
	
	@Test
	public void draw_testLeafUnsupportedOperation() {
		try {
			line.add(new Line());
		} catch(Exception e) {
			Assert.assertTrue("A line object should not support add().", e instanceof UnsupportedOperationException);
		}
	}
}
