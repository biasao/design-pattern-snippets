package com.blabs.designpatterns.composite;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractGraphicComposite implements Graphic {
	protected List<Graphic> graphics;
	
	public AbstractGraphicComposite() {
		graphics = new ArrayList<Graphic>();
	}
	
	public void add(Graphic graphic) {
		graphics.add(graphic);
	}
	
	public void remove(Graphic graphic) {
		graphics.remove(graphic);
	}
	
	public void getChild(int i) {
		graphics.get(i);
	}
}
