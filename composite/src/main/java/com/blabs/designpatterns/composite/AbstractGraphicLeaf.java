package com.blabs.designpatterns.composite;

public abstract class AbstractGraphicLeaf implements Graphic {
	public void add(Graphic graphic) {
		throw new UnsupportedOperationException("This is a leaf, can't add any child.");
	}

	public void remove(Graphic graphic) {
		throw new UnsupportedOperationException("This is a leaf, can't remove any child.");
	}

	public void getChild(int i) {
		throw new UnsupportedOperationException("This is a leaf, can't get any child.");
	}
}
