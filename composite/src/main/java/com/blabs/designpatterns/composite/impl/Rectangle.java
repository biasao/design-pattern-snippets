package com.blabs.designpatterns.composite.impl;

import com.blabs.designpatterns.composite.AbstractGraphicLeaf;

public class Rectangle extends AbstractGraphicLeaf {

	public void draw() {
		System.out.println("Drawing a rectangle...");
	}
}
