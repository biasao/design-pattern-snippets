package com.blabs.designpatterns.composite.impl;

import com.blabs.designpatterns.composite.AbstractGraphicLeaf;

public class Line extends AbstractGraphicLeaf {

	public void draw() {
		System.out.println("Drawing a line...");
	}
}
