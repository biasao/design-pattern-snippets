package com.blabs.designpatterns.composite;

public interface Graphic {
	void draw();
	void add(Graphic graphic);
	void remove(Graphic graphic);
	void getChild(int i);
}
