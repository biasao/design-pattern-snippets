package com.blabs.designpatterns.composite.impl;

import com.blabs.designpatterns.composite.AbstractGraphicComposite;
import com.blabs.designpatterns.composite.Graphic;

public class Picture extends AbstractGraphicComposite {

	public void draw() {
		for(Graphic graphic : graphics) {
			graphic.draw();
		}
	}
}
