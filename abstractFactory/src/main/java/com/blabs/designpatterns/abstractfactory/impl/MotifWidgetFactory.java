package com.blabs.designpatterns.abstractfactory.impl;

import com.blabs.designpatterns.abstractfactory.WidgetFactory;
import com.blabs.designpatterns.abstractfactory.widgets.impl.MotifScrollBar;
import com.blabs.designpatterns.abstractfactory.widgets.impl.MotifWindow;

public class MotifWidgetFactory implements WidgetFactory {

	public MotifScrollBar createScrollBar() {
		MotifScrollBar scrollBar = new MotifScrollBar();
		
		return scrollBar;
	}

	public MotifWindow createWindow() {
		MotifWindow window = new MotifWindow();
		
		return window;
	}

}
