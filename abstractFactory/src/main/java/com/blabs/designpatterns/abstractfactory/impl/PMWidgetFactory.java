package com.blabs.designpatterns.abstractfactory.impl;

import com.blabs.designpatterns.abstractfactory.WidgetFactory;
import com.blabs.designpatterns.abstractfactory.widgets.impl.PMScrollBar;
import com.blabs.designpatterns.abstractfactory.widgets.impl.PMWindow;

public class PMWidgetFactory implements WidgetFactory {

	public PMScrollBar createScrollBar() {
		PMScrollBar scrollBar  = new PMScrollBar();
		
		return scrollBar;
	}

	public PMWindow createWindow() {
		PMWindow window = new PMWindow();
		
		return window;
	}
}
