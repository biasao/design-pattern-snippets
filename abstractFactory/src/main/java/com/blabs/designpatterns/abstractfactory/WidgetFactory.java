package com.blabs.designpatterns.abstractfactory;

import com.blabs.designpatterns.abstractfactory.widgets.ScrollBar;
import com.blabs.designpatterns.abstractfactory.widgets.Window;

public interface WidgetFactory {
	ScrollBar createScrollBar();
	Window createWindow();
}
