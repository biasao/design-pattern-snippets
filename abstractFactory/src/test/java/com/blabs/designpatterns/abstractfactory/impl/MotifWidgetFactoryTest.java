package com.blabs.designpatterns.abstractfactory.impl;

import junit.framework.Assert;

import org.junit.Test;

import com.blabs.designpatterns.abstractfactory.WidgetFactory;
import com.blabs.designpatterns.abstractfactory.widgets.ScrollBar;
import com.blabs.designpatterns.abstractfactory.widgets.Window;
import com.blabs.designpatterns.abstractfactory.widgets.impl.MotifScrollBar;
import com.blabs.designpatterns.abstractfactory.widgets.impl.MotifWindow;

public class MotifWidgetFactoryTest {

	private WidgetFactory factory = new MotifWidgetFactory();
	
	@Test
	public <T> void createScrollBar_verifyReturnedType() {
		ScrollBar scrollBar = factory.createScrollBar();
		
		Assert.assertTrue(scrollBar instanceof MotifScrollBar);
	}

	@Test
	public void createWindow_verifyReturnedType() {
		Window window = factory.createWindow();
		
		Assert.assertTrue(window instanceof MotifWindow);
	}
}
